package main

import (
    "net/http"
	"fmt"
)

func main() {
    http.HandleFunc("/a", handler)
    http.ListenAndServe(":3000", nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Hello World!")
}
