# Go langage webserver

## Go言語+Nginx+ホットリロード環境

golang の組み込みライブラリ `net/http` を使ってFastCGIとして動かし `Nginx` でリバースプロキシして接続させた．
またホットリロード環境として `realize`(http://github.com/oxequa/realize) を利用した．

## 設定
`golang/app/main.go` の `http.ListenAndServe` でポートを指定します．

ここで指定したポートに合わせて `docker-compose.yml` で開放ポートを指定．

またアプリケーションサーバのコンテナ名を任意に指定し，`nginx/nginx.conf` の `proxy_pass` にポートと合わせて接続先を指定する．
